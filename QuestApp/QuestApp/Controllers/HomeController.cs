﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QuestApp.Models;
using QuestApp.Services;
using System.Web;
using QuestApp.Data;
using Microsoft.AspNetCore.Cors;
using QuestApp.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace QuestApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        
        private readonly IBookingRepository _bookingRepository;
        private readonly ITreatmentRepository _TreatmentRepository;

        private AppDbContext _appDbContext;
        public EditViewModel EditViewModel = new EditViewModel();

        public HomeController(IBookingRepository bookingRepository,AppDbContext appDbContext)
        {
            this._bookingRepository = bookingRepository;
            _appDbContext = appDbContext;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Booking model,LU_Treatment models)
        {
            var obj = new Booking();
            obj.Name = model.Name;
            obj.Mobile = model.Mobile;
            obj.Email = model.Email;
            obj.Note = model.Note;
            obj.BookedDate = model.BookedDate;
            _bookingRepository.Add(obj);
            return View();
        }


        [Authorize]
        public IActionResult Create()
        {
            List<LU_Treatment> li = new List<LU_Treatment>();
            li = _appDbContext.Treatments.ToList();
            ViewBag.ListOfItems = li;
            List<LU_TimeSlot> lii = new List<LU_TimeSlot>();
            lii = _appDbContext.TimeSlots.ToList();
            ViewBag.ListOfTimes = lii;

            return View();
        }
        [HttpPost]
        public IActionResult Create(Booking model)
        {
           
            if (ModelState.IsValid)
            {
                var obj = new Booking();
                obj.Name = model.Name;
                obj.Mobile = model.Mobile;
                obj.Email = model.Email;
                obj.Note = model.Note;
                obj.BookedDate = model.BookedDate;
                _bookingRepository.Add(model);
                return RedirectToAction("Inf");
            }
            else {
                return View();
            }
        }
        [Authorize]
        //GetAll Bookings
        public IActionResult Data()
        {
            var model = _bookingRepository.GetAll();
            return View(model);
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }
        [Authorize]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
        [Authorize]
        public async Task<IActionResult> Inf(string search,int searchNum)
        {
          
                //var model = from m in _appDbContext.Bookings
                //             select m;
                var model = _bookingRepository.GetAll();
                if (!String.IsNullOrEmpty(search))
                {
                    model = model.Where(s => s.Name.Contains(search));
                    Console.WriteLine("/////////////////" + model);
                }
                if (searchNum > 0)
                {
                    model = model.Where(m => m.Mobile.Equals(searchNum));
                    Console.WriteLine("/////////////////" + model);
                }
                foreach (var obj in model)
                {
                    var treatId = obj.treatmentId;
                    //obj.Treatment = new LU_Treatment();
                    obj.Treatment = _appDbContext.Treatments.Single(e => e.id == treatId);
                    obj.TimeSlot = _appDbContext.TimeSlots.Single(e => e.Id == obj.TimeSlotId);
                }
                return View(model);
           
        }
        [Authorize]
        public IActionResult Details(int id)
        {
           var model =  _bookingRepository.Get(id);
            var treatId = model.treatmentId;
            model.Treatment = _appDbContext.Treatments.Single(e => e.id == treatId);
            model.TimeSlot = _appDbContext.TimeSlots.Single(e => e.Id == model.TimeSlotId);
            return PartialView("Details",model);
        }
        [Authorize]
        public IActionResult Delete(int id)
        {
            _bookingRepository.Delete(id);
            return RedirectToAction("Inf");

        }
        [Authorize]
        public IActionResult Edit(int id)
        {
            List<LU_Treatment> li = new List<LU_Treatment>();
            li = _appDbContext.Treatments.ToList();
            ViewBag.ListOfItems = li;
            List<LU_TimeSlot> lii = new List<LU_TimeSlot>();
            lii = _appDbContext.TimeSlots.ToList();
            ViewBag.ListOfTimes = lii;
            var model = _bookingRepository.Get(id);
            EditViewModel.Name = model.Name;
            EditViewModel.Note = model.Note;
            EditViewModel.Mobile = model.Mobile;
            EditViewModel.Email = model.Email;
            EditViewModel.BookedDate = model.BookedDate;
            EditViewModel.BookingId = model.BookingId;
            EditViewModel.TimeSlotId = model.TimeSlotId;
            EditViewModel.treatmentId = model.treatmentId;
            return View(EditViewModel);
        }

        [HttpPost]
        public IActionResult EditUser(int id,Booking book)
        {
            var model = _bookingRepository.Edit(id, book);
            EditViewModel.Name = model.Name;
            EditViewModel.Note = model.Note;
            EditViewModel.Mobile = model.Mobile;
            EditViewModel.Email = model.Email;
            EditViewModel.BookedDate = model.BookedDate;
            return RedirectToAction("Inf");
        }
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
